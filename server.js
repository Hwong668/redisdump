/**
 * Created by Herbert on 11/17/2014.
 */
'use strict';
var async = require('async');
var _ = require('lodash');
var fs = require('fs');
var redis = require('redis');
var redis_client = redis.createClient(6379, 'mmioredis.2i2z0n.0001.usw2.cache.amazonaws.com');
//var redis_client = redis.createClient(6379, '127.0.0.1');
var writeStream = fs.createWriteStream('redisDump.txt', {'flags':'a'});

/*
//get all the sorted set for search
//'contract\\-rate:*:search:*'
//'contract\\-names:*:search:*'
var dumpSearchKeys = function(keyPattern, callback){
    redis_client.keys(keyPattern, function (err, replies) {
        async.forEach(replies,function(r, cb){
            redis_client.zrange(r, 0, -1, 'withscores' ,function(err2, results){
                for(var n=0; n<results.length - 2; n=n+2){
                    var str = 'ZADD ' + r + ' ' + results[n+1] + ' ' + results[n] + '\r\n';
                    fs.writeFileSync('redisDump.txt', str, {'flags':'a'});
                    //writeStream.write(str);
                    //writeStream.write('ZADD ' + r + ' ' + results[n+1] + ' ' + results[n] + '\r\n');
                    if(n+2 > results.length){
                        return cb();
                    }
                }

            });
        }, function (cb2) {
            return callback(null);
        });
    });
};
//get all the hash data
//'contract-rate:data'
//'contract-names:data'
var dumpData = function(dataPattern, callback){
    redis_client.hgetall(dataPattern, function(err,replies){
        if(!replies){
            return callback(null);
        }
        var items = Object.keys(replies);
        async.forEach(items, function (key, cb) {
            var value = replies[key];
            var str = 'HSET ' + 'contract-rate:data ' + key + ' "' + value.replace(/"/g,'\'') + '"\r\n';
            fs.writeFileSync('redisDump.txt', str, {'flags':'a'});
            //writeStream.write(str);
            //writeStream.write('HSET ' + 'contract-rate:data ' + key + ' "' + value.replace(/"/g,'\'') + '"\r\n');
            return cb();
        },function(cb2){
            return callback(null);
        });
    });
};

async.series([
    function(callback){
        dumpSearchKeys('contract\\-rate:*:search:*', callback);
    },
    function(callback){
        dumpData('contract-rate:data', callback);
    },
    function(callback){
        dumpSearchKeys('contract\\-names:*:search:*', callback);
    },
    function(callback){
        dumpData('contract-names:data', callback);
    },
     function (callback) {
     console.log('Done!');
     process.exit(0);
     }
]);*/


redis_client.keys('contract\\-rate:*:search:*', function (err, replies) {
    _.forEach(replies,function(r){
        redis_client.zrange(r, 0, -1, 'withscores' ,function(err2, results){
            for(var n=0; n<results.length - 2; n=n+2){
                writeStream.write('ZADD ' + r + ' ' + results[n+1] + ' ' + results[n] + '\r\n');
            }
        })
    })
});
redis_client.hgetall('contract-rate:data', function(err,replies){
    Object.keys(replies).forEach(function (key) {
        var value = replies[key];
        writeStream.write('HSET ' + 'contract-rate:data ' + key + ' ' + '\'' + value.replace(/'/g,'"') + '\'' + '\r\n');
    })
});


redis_client.keys('contract\\-names:search:*', function (err, replies) {
    _.forEach(replies,function(r){
        redis_client.zrange(r, 0, -1, 'withscores' ,function(err2, results){
            for(var n=0; n<results.length - 2; n=n+2){
                writeStream.write('ZADD ' + r + ' ' + results[n+1] + ' ' + results[n] + '\r\n');
            }
        })
    })
});
redis_client.hgetall('contract-names:data', function(err,replies){
    Object.keys(replies).forEach(function (key) {
        var value = replies[key];
        writeStream.write('HSET ' + 'contract-names:data ' + key + ' ' + '\'' + value.replace(/'/g,'"') + '\'' + '\r\n');
        //writeStream.write('HSET ' + 'contract-names:data ' + key + ' "' + value.replace(/'/g,'"') + '"\r\n');
    })
});

//add the contract wizard data in as well